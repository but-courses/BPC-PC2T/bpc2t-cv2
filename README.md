# Course: BPC2T - seminar 2
This project provides a solution for seminar 2. Includes topics like:
1. Class creation 
2. Java access modifiers
3. Constructos (implicit, with parameters)
4. Static keyword
5. Main class
6. Setters and Getters
7. This keyword

## Authors

email | Name 
------------ | -------------
pavelseda@email.cz | Šeda Pavel

## Starting up Project
Just import it in your selected IDE (Eclipse, IntelliJ IDEA, Netbeans, ...)

## Used Technologies
The project was built and tested with these technologies, so if you have any unexpected troubles let us know.

```
Maven         : 3.3.9
Java          : OpenJDK 11
```