package cz.vutbr.feec;

/**
 * 
 * @author Pavel Seda
 *
 */
public class App {

	public static void main(String[] args) {
		App app1 = new App();
		app1.createEmployeesInstances();
		app1.iterateThroughEmployeeArray();

		MathOperations.computeQuadraticEquation(1, 5, 4);

		// Vypsání počtu objektů typu Employee, kteří byli vytvořeni
		System.out.println(Employee.getNumberOfEmployees());
	}

	/**
	 * <ul>
	 * <li>vytvoření 5ti instancí třídy Employee dle zadání,</li>
	 * <li>u první instance vytvoření prázdného konstruktoru a nasetování zbylých
	 * hodnot pomocí setter metod</li>
	 * <li>u dalsich 4 nasetování metod v inicializaci v konstruktoru</li>
	 * </ul>
	 *
	 */
	public void createEmployeesInstances() {
		Employee emp1 = new Employee();
		emp1.setAge(30);
		emp1.setEmail("test@email.cz");
		emp1.setInterests(new String[] { "fotbal", "tenis" });
		emp1.setName("Pavel Seda");
		emp1.setPassword("mojeSuperTajneHeslo".toCharArray());
		Employee emp2 = new Employee("Marek Ztraceny", 20, "ztratilJsemSe".toCharArray(), "test2@email.cz",
				new String[] { "table tenis" });
		Employee emp3 = new Employee("Christina Aguilera", 20, "voiceWithin".toCharArray(), "test3@email.cz",
				new String[] { "table tenis" });
		Employee emp4 = new Employee("Elton John", 20, "sorrySeemsToBeTheHardestWord".toCharArray(), "test4@email.cz",
				new String[] { "table tenis" });
		Employee emp5 = new Employee("Milos Zeman", 20, "jsemPrezident".toCharArray(), "test5@email.cz",
				new String[] { "cigaretka na dva tahy" });
	}

	/**
	 * Vytvoření pole o 2 zaměstnancích a iterace skrze toto pole. Následné vypsání
	 * emailu u každého zaměstnance.
	 */
	public void iterateThroughEmployeeArray() {
		// pole o velikosti 2
		Employee[] employees = new Employee[2];
		employees[0] = new Employee("Jiri Ovcacek", 20, "jsemCtveracek".toCharArray(), "ovca@email.cz",
				new String[] { "the defender" });
		employees[1] = new Employee("Richard Muller", 20, "somNahy".toCharArray(), "risa@email.cz",
				new String[] { "zpev" });

		// iterace skrze pole vytvorene vyse a vypsani emailu kazdeho zamestnance
		for (int i = 0; i < employees.length; i++) {
			System.out.println(employees[i].getEmail());
		}
	}
}
