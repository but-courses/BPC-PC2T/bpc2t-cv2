package cz.vutbr.feec;

import java.util.Arrays;

/**
 * Třída reprezentující zaměstnance v systému.
 * 
 * @author Pavel Seda
 *
 */
public class Employee {

	private String name;
	private int age;
	private char[] password;
	private String email;
	private String[] interests;
	private static int numberOfEmployees;

	public Employee() {
		this.name = "";
		this.age = 0;
		this.password = null;
		this.email = "";
		this.interests = null;
		Employee.numberOfEmployees++;
	}

	public Employee(String name, int age, char[] password, String email, String[] interests) {
		super();
		this.name = name;
		this.age = age;
		this.password = password;
		this.email = email;
		this.interests = interests;
		Employee.numberOfEmployees++;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public char[] getPassword() {
		return password;
	}

	public void setPassword(char[] password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String[] getInterests() {
		return interests;
	}

	public void setInterests(String[] interests) {
		this.interests = interests;
	}

	public static int getNumberOfEmployees() {
		return numberOfEmployees;
	}

	public static void setNumberOfEmployees(int numberOfEmployees) {
		Employee.numberOfEmployees = numberOfEmployees;
	}

	@Override
	public String toString() {
		return "Employee [name=" + name + ", age=" + age + ", password=" + Arrays.toString(password) + ", email="
				+ email + ", interests=" + Arrays.toString(interests) + "]";
	}

}
