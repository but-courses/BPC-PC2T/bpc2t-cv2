package cz.vutbr.feec;

/**
 * Class providing mathematical operations.
 * 
 * @author Pavel Seda
 *
 */
public class MathOperations {

	/**
	 * Metoda pro výpočet kvadratické rovnice. Vypisuje kořeny kvadratické rovnice
	 * do konzole.
	 * 
	 * @param a vstupní parametr a reprezentující neznámou v zadané rovnici
	 * @param b vstupní parametr b reprezentující neznámou v zadané rovnici
	 * @param c vstupní parametr c reprezentující neznámou v zadané rovnici
	 */
	public static void computeQuadraticEquation(int a, int b, int c) {
		double result = b * b - 4.0 * a * c;

		if (result > 0.0) {
			double r1 = (-b + Math.pow(result, 0.5)) / (2.0 * a);
			double r2 = (-b - Math.pow(result, 0.5)) / (2.0 * a);
			System.out.println("The roots are " + r1 + " and " + r2);
		} else if (result == 0.0) {
			double r1 = -b / (2.0 * a);
			System.out.println("The root is " + r1);
		} else {
			System.out.println("The equation has no real roots.");
		}
	}
}
